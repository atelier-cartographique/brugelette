import csv

street_name = []
row_grid = []
col_grid = []
with open('street_index.csv') as csvfile:
    r = csv.DictReader(csvfile)
    for row in r:
        street_name.append(row['name'])
        col_grid.append(row['col'])
        row_grid.append(row['row'])

unique_street_name = set(street_name)

# Split the street names
unique_street_name_splitted = []
for i, s in enumerate(unique_street_name):
    d = {}
    item = s.split(' ')
    d['name'] = item[-1]
    d['article'] = ' '.join(item[0:-1])
    unique_street_name_splitted.append(d)

# Sort by the 'name' key
unique_street_name_splitted_sorted = sorted(unique_street_name_splitted, key=lambda d: d['name'])
print(unique_street_name_splitted_sorted)

# Join the grid col and row number
with open('street_index_filtered.csv', 'w') as csvfile:
    fieldnames = ['street_name', 'cells']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for i, usn in enumerate(unique_street_name_splitted_sorted):
        full_name = usn['article'] + ' ' + usn['name']
        grid_cells = []
        for j, sn in enumerate(street_name):
            if sn == usn['article'] + ' ' + usn['name'] :
                grid_cells.append(str(col_grid[j] + str(row_grid[j])))
        writer.writerow({'street_name': full_name, 'cells': ','.join(sorted(set(grid_cells)))})
