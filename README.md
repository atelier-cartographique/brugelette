# Une carte de Brugelette

La Commune de Brugelette est confrontée comme toutes les communes au problème récurrent de la mise à jour et de l'édition du plan communal. Après avoir envisagé plusieurs solutions compliquées, souvent onéreuses, et qui font appel à la publicité, la Commune a décidé de se tourner vers une autre solution, basée sur Open Street Map. L'idée est de cartographier le territoire communal collectivement, avec les habitants et par les habitants. Cette carte sera imprimée à 2000 exemplaires et à être distribuée dans les foyers de la Commune. Le travail de cartographie sera aussi disponible sur le web. Cette carte sera enrichie au fur et à mesure.

# Cartographie


## Grille et index des rues

Une grille a été créée pour l'afficher comme couche dans la carte et pouvoir faire un index des rues.

### Création de la grille

Dans QGIS, créer une grille avec l'outil Vector > Research tools > Create grid:

* type: rectangle
* extent: à choisir sur la carte, par ex. 111000, 118500, 139500, 147500 [EPSG: 31370]
* horizontal spacing = vertical spacing: 500 (m)
* sauvegarder la grille


La grille créée contient les coordonnées bottom, top, left et right dans la table d'attributs. On ajoute des noms de lignes et colonnes en ouvrant la calculette de champ et en créant 2 nouveaux champs:

1) row, comme integer, avec la formule:
```
-("top"-minimum("top"))/500 + 16
```
où `16` est le nombre de lignes et `500` la résolution de la grille.

2) col, comme string, avec la formule (avec 16 lignes):
```
CASE
    WHEN "left" = minimum("left") THEN 'A'
    WHEN "left" = minimum("left")+500 THEN 'B'
    WHEN "left" = minimum("left")+1000 THEN 'C'
    WHEN "left" = minimum("left")+1500 THEN 'D'
    WHEN "left" = minimum("left")+2000 THEN 'E'
    WHEN "left" = minimum("left")+2500 THEN 'F'
    WHEN "left" = minimum("left")+3000 THEN 'G'
    WHEN "left" = minimum("left")+3500 THEN 'H'
    WHEN "left" = minimum("left")+4000 THEN 'I'
    WHEN "left" = minimum("left")+4500 THEN 'J'
    WHEN "left" = minimum("left")+5000 THEN 'K'
    WHEN "left" = minimum("left")+5500 THEN 'L'
    WHEN "left" = minimum("left")+6000 THEN 'M'
    WHEN "left" = minimum("left")+6500 THEN 'N'
    WHEN "left" = minimum("left")+7000 THEN 'O'
    WHEN "left" = minimum("left")+7500 THEN 'P'
END
```
où `500` est la résolution de la grille (carrée).


### Création de l'index des rues

1) Dans QGIS, à partir de la couche osm_lines, sélectionner les rues nommées (et éviter les allées de parking de Pairi Daisa)
```
"highway" IS NOT NULL AND "name" IS NOT NULL AND ("service"  !=  'parking_aisle' OR "service" IS NULL)
```
et sauvegarder cette couche -> named_highways.shp

2) Clipper cette couche par l'emprise de Brugelette -> named_highways_clip_brugelette.shp

3) Faire une intersection entre cette couche et la couche de la grille
* input: named_highways_clip_brugelette
* overlay: grid
* selectionner les champs des inputs (osm_id, highway, name) et de l'overlay (col, row) pour que ce soit plus propre.

Sauvegarder la couche sous fichier csv (après avoir filtré les "col" ou "row" non null).

Dans ce fichier csv, les noms de rues sont dupliqués autant de fois que la rue croise des cellules de la grille. Gérer les duplicas pour passer de:
```
"Rue des champs", "A1"
"Rue des champs", "A2"
```
à:
```
"Rue des champs", "A1, A2"
```

Il y aussi des duplicas à simplement éliminer (même nom de rue et indice de cellule) lorsqu'il y a plusieurs segments d'une même rue au sein d'une cellule. Pour filtrer ces duplicas, il y a un script Python:

```
python make_street_index.py
```

# Comparaison ICAR vs OSM

Au 27 mai 2019, il y a 103 rues dans la BD ICAR et 136 rues dans OSM à Brugelette.

## Rues dans OSM
Pour obtenir un une liste de rues nommées, partir de la couche osm_lines et effectuer la requête `"highway" IS NOT NULL AND "name" IS NOT NULL`. Ensuite, clipper sur l'emprise de la commune et obtenir les valeurs uniques (outil dans QGIS: Vector > Analysis Tools).

En outre:
* supprimer les voies nommées Allée XXX (parc Paradisio)

Différences observées (OSM vs ICAR):

* Avenue de Bragues vs Avenue de Brague: erreur OSM à corriger ?
* avenue de Gages vs Avenue de Gages: erreur OSM à corriger en Avenue de Gages: OK
* Avenue de l'Église vs Avenue de l'Eglise: erreur ICAR
* Avenue Saint Martin vs Avenue Saint-Martin: erreur OSM à corriger ?
* Bois d'Herimé vs Bois d'Hérimé: erreur OSM à corriger ?
* Grand-Chemin vs Grand Chemin: erreur OSM à corriger ?
* Place de Keyser vs Place De Keyser: erreur OSM à corriger ?
* Rue Colonel aviateur Daumerie vs Rue Colonel Aviateur Daumerie: erreur OSM à corriger: OK
* Chemin de Fouleng vs Rue de Fouleng: erreur OSM à corriger ?
* rue de la Chapelle Bruno vs Rue de la Chapelle Bruni: erreur OSM à corriger: OK
* rue des Vaillants vs Rue des Vaillants: erreur OSM à corriger: OK
* rue Dieudonné du Val de Beaulieu vs Rue Dieudonné du Val de Baulien: erreur OSM à corriger: OK
* rue du bois d'Hérimé vs Bois d'Hérimé: erreur OSM à corriger ?
* Rue du Marronier vs Rue du Marronnier: erreur OSM à corriger: OK
* Rue du Moulin à eau vs Rue du Moulin à Eau: erreur OSM à corriger: OK
* rue le Coucou et Rue le Coucou dans OSM: erreur OSM à corriger: OK
* rue Liévin Thésin vs Rue Liévin Thésin: erreur OSM à corriger: OK
* Rue Saint-lambert vs Rue Saint-Lambert: erreur OSM à corriger: OK
* Rue Les Trieux vs Rue Trieux: erreur OSM à corriger ?
* Les tilleuils vs Les Tilleuls: erreur OSM à corriger: OK
* Rue du Tonquin vs le Tonkin: erreur OSM à corriger ?
* Rue Le Coucou vs Le Coucou: erreur OSM à corriger ?

Autres erreurs OSM:
* "chemin innommé": nom à supprimer: OK
* "Voie piétonne": nom à supprimer: OK
* "sentier des Sammes": capitaliser: OK
* possible nom similaire: route des Wespellières vs Les Wespellières.
* "Rue de Cambron-Castreau": OK


Absent OSM:
* Chemin Gabrielle Petit
* Cour Roucloux: OK, ajouté
* Rue d'Ath à Soignies
* Rue d'Hembise : OK
