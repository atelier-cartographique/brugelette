# Présentation Brugelette 20 juin 2019

## Impression

- assemblée intéressée et contente du travail présenté

## Point relevés

### Que faut-il afficher sur la carte ?

Lors de la présentation il a été évoqué la question de ce qui serait intéressant d'afficher sur la carte.

Voici les propositions qui ont été évoquées :

- bulles à verre
- terrains de sport
    - de foot (ok)
    - de basket (ok)
    - de **hockey** (manquant)
    - de **cyclo-cross** (manquant)
- info sur le patrimoine
    - rien n'a encore été encodé

- **lieux-dits**
    - chateau d'Âtre (question apparait-il déjà ? sinon pourquoi ? )
    - autres
        - voir les données qui se trouvent sur OSM
        - nous avons aussi demandé à l'assemblée pour avoir une liste de lieux-dits à afficher

### Avoir une version digitale

Il a été évoqué d'afficher

- les commerces,
- les associations.

Pour ces données il semble plus intéressant de faire des version digitales car l'info bouge.
Aussi le but de la carte n'est pas de faire la promotion des commerces.

Nous avons dévié sur le sujet de créer différentes cartes thématiques.

### Phase d'impression

- 1ère impression / édition 2019 :
    - impression de 2000 cartes
        - 1600 seront distribuées en "toutes-boîtes"
        - 400 distribuées par la commune (office du tourisme, ...)
- 2ème impression (édition 2020) :
    - une fois qu'il n'y a plus de cartes à distribuer
    - idée de faire une mise à jour de la carte selon le retour

### Texte de présentation à mettre sur la carte

Ne pas oublier de demander à Stéphanie le texte de présentation à mettre sur la carte
(explication du projet, qui a participé, qui contacter, ...)
